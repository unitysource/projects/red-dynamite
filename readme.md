# Red Dynamite (Hyper-Casual Game)

![Game Logo](/images/logo.png)

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- [Contributing](#contributing)

## About the Game

Jump with click by screen. Complete levels

## Gameplay

Finish levels by jumping gallop

![Gameplay GIF](/images/gameplay.gif)

## Features

Key features of game:
- Interesting gameplay mechanic

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/red-dynamite.git`
2. Navigate to the game directory: `cd red-dynamite`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**
- Tap to move

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
